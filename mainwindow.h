#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "eccrypt.h"
#include "kuznyechik.h"
#include <QMainWindow>
#include <QMessageBox>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    eccrypt_curve_t EC; // параметры кривой

    int A_k; // закрытые ключ
    int B_k;

    eccrypt_point_t P; // точка эллиптической кривой
    eccrypt_point_t A_Qa; // открытый ключ Алисы на стороне Алисы
    eccrypt_point_t A_Qb; // открытый ключ Боба на стороне Алисы
    eccrypt_point_t B_Qb; // открытый ключ Боба на стороне Боба
    eccrypt_point_t B_Qa; // открытый ключ Алисы на стороне Боба
    eccrypt_point_t A_Key; // общий ключ на стороне Алисы
    eccrypt_point_t B_Key; // общий ключ на стороне Боба


    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:

    void on_Elliptic_curve_param_accept_clicked(); // задать параметры кривой

    void on_A_k_random_clicked(); // генерировать случайный закрытый ключ на стороне Алисы

    void on_A_k_accept_clicked(); // на стороне Алисы принять закрытый ключ

    void on_B_k_random_clicked(); // генерировать случайный закрытый ключ на стороне Боьа

    void on_B_k_accept_clicked(); // на стороне Боба принять закрытый ключ

    void on_A_send_Qa_Bob_clicked(); // Алиса отправляет открытый ключ Бобу

    void on_B_send_Qb_Alice_clicked(); // Боб отправляет открытый ключ Алисе

    void on_A_calculate_Key_clicked(); // Алиса вычисляет общий ключ

    void on_B_calculate_Key_clicked(); // Боб  вычисляет общий ключ

    void on_A_send_msg_clicked(); // Алиса отправляет сообщение Бобу , т.е. сообщение шифруется у Алисы и дешифруется у Боба

    void on_B_send_msg_clicked(); // Боб отправляет сообщение Алисе

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
